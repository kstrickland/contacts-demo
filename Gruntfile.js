module.exports = function(grunt) {

	var versionTime = new Date().getTime();
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		copy: {
			all: {
				files: [
					{
						expand: true,
						src: [
							'bower_components/backbone/backbone.js',
							'bower_components/marionette/lib/backbone.marionette.js',
							'bower_components/requirejs/require.js',
							'bower_components/requirejs-text/text.js',
							'bower_components/underscore/underscore.js',
							'bower_components/jquery/dist/jquery.js',
							'bower_components/bootstrap/dist/js/bootstrap.js'
						],
						dest: 'src/lib/',
						flatten: true,
						filter: 'isFile'
					},
					{
						expand: true,
						src: [
							'bower_components/bootswatch-dist/css/bootstrap.css'
						],
						dest: 'src/css/',
						flatten: true,
						filter: 'isFile'
					},
					{
						expand: true,
						src: [
							'bower_components/bootstrap/dist/fonts/*.*'
						],
						dest: 'src/fonts/',
						flatten: true,
						filter: 'isFile'
					}
				]
			}
		},
		clean: {
			prep: [
				'bower_components/',
				'src/lib/',
				'src/css/*.*',
				'!src/css/site.css',
				'src/fonts/',
				'node_modules/'
			],
			bower: [
				'bower_components/'
			],
			buildDev: [
				'notesin9-contacts/WebContent/app/',
				'notesin9-contacts/WebContent/build.txt'
			]
		},
		replace: {
			buildJs: {
				src: ['src/config/config.js'],
				overwrite: true,
				replacements: [
					{
						from: /([0-9]{13,})/,
						to: versionTime
					}
				]
			},
			buildHtmlDev: {
				src: ['notesin9-contacts/WebContent/index.html'],
				overwrite: true,
				replacements: [
					{
						from: '<script data-main="config/config" src="lib/require.js"></script>',
						to: '<script data-main="lib/site" src="lib/require.js"></script>'
					}
				]
			}
		},
		watch: {
			options: {
				spawn: false,
				cwd: 'src'
			},
			srcDir: {
				files: ['**/*.*', '!config/config.js'],
				tasks: ['buildDev']
			}
		},
		connect: {},
		requirejs: {
			buildDev: {
				options: {
					appDir: 'src/',
					baseUrl: './',
					mainConfigFile: 'src/config/config.js',
					logLevel: 2,
					removeCombined: true,
					findNestedDependencies: true,
					dir: 'notesin9-contacts/WebContent/',
					optimizeCss: 'none',
					inlineText: true,
					optimize: 'none',
					fileExclusionRegExp: /^(node_modules$)/,
					preserveLicenseComments: false,
					generateSourceMaps: true,
					modules: [
						{
							name: 'lib/site',
							include: ['config/config'],
							create: true
						}
					]
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-requirejs');
	grunt.loadNpmTasks('grunt-text-replace');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.registerTask('setup',[
		'copy:all',
		'clean:bower'
	]);
	grunt.registerTask('buildDev',[
		'replace:buildJs',
		'requirejs:buildDev',
		'replace:buildHtmlDev',
		'clean:buildDev'
	]);
};
