/**
 * These are basic and default settings for configuration of the application
 * AUTHENTICATION_URL - The url for authentication
 * LOGOUT_URL - The url to log out of the application
 */
define({
        AUTHENTICATION_URL: '/names.nsf?login',
        LOGOUT_URL: '/names.nsf?logout'
});
