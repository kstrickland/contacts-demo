require.config({
	baseUrl: './',
	urlArgs: 'open&bustCache=' + 'v1.0.1429908859772',
    shim: {
        jquery: {
            exports: '$'
        },
        bootstrap: {
            deps: ['jquery']
        },
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: ['jquery', 'underscore'],
            exports: 'Backbone'
        },
        marionette: {
            deps: ['backbone'],
            exports: 'Marionette'
        }
    },
    paths: {
        underscore: 'lib/underscore',
        backbone: 'lib/backbone',
        marionette: 'lib/backbone.marionette',
        jquery: 'lib/jquery',
        bootstrap: 'lib/bootstrap',
        text: 'lib/text',
        application: 'app/application',
        settings: 'config/settings'
    },
    text: {
        env: 'xhr'
    }
});

require([
    'application',
    'bootstrap'
], function(App, Bootstrap) {
    ContactApp = new App();
    ContactApp.start();
});