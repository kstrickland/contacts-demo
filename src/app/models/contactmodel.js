define([
	'backbone'
], function(Backbone) {

	// Contact Model
	var Contact = Backbone.Model.extend({
		idAttribute: '@unid',
		/**
		 * The URL to use for all REST requests
		 * @return {String} The URL for the REST service to find this contact
		 */
		url: function() {
			if (this.isNew()) {
				return 'api/data/documents?Form=contact';
			}else{
				return Backbone.Model.prototype.url.apply(this);
			}
		},
		/**
		 * Keep track if this model has been saved or not
		 * @type {Boolean}
		 */
		isDirty: false,
		/**
		 * The base URL to this model
		 * @type {String}
		 */
		urlRoot: 'api/data/documents/unid',
		/**
		 * Setup some default values for this model
		 * @type {Object}
		 */
		defaults: {
			FirstName: '',
			LastName: '',
			Email: '',
			HomePhone: '',
			CellPhone: '',
			Supervisor: ''
		},
		/**
		 * The Constructor for this model. Sets up some model event listeners to control the isDirty property
		 */
		initialize: function() {
			var that = this;
			this.on({
				'request': function() {
					that.isDirty = false;
				},
				'sync': function() {
					that.isDirty = false;
				},
				'change': function() {
					that.isDirty = true;
				}
			});
		},
		/**
		 * Get this contact's full name
		 * @return {String} The FirstName + " " + LastName of this contact
		 */
		getFullName: function() {
			return this.get('FirstName') + ' ' + this.get('LastName');
		}
	});

	return Contact;

});
