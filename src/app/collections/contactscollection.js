define([
	'backbone',
	'app/models/contactmodel.js'
], function(Backbone, Contact) {

	// Contacts Collection
	var ContactsCollection = Backbone.Collection.extend({
		/**
		 * The REST url to use to fetch all the contacts
		 * @type {String}
		 */
		url: 'api/data/collections/name/contacts',
		/**
		 * The Model to use for each entry in this collection
		 * @type {[type]}
		 */
		model: Contact,
		/**
		 * The Contstructor
		 */
		initialize: function() {
		}
	});

	return ContactsCollection;

});
