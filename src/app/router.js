define([
	'backbone',
	'app/views/contactstableview.js',
	'app/views/contactview.js',
	'app/views/home.js'
], function(Backbone, ContactsTableView, ContactView, HomeView) {

	// The Router
	var AppRouter = Backbone.Router.extend({
		/**
		 * This defines our routes. These tell Backbone.Marionette what to do when one of these routes
		 * is visited via a URL
		 * @type {Object}
		 * @property {String} home - The Home route
		 * @property {String} contacts - The contacts page (list of contacts)
		 * @property {String} contacts/:id - An Existing contact
		 * @property {String} contacts/:new - A New Contact
		 */
		routes: {
			'': 'home',
			'contacts': 'contacts',
			'contacts/:id': 'contacts',
			'contacts/:new': 'contacts'
		},
		/**
		 * Fired when the home route is visited. Creates a HomeView and adds it to the CONTENT region.
		 * We then update the active status of the links in the navbar.
		 */
		home: function() {
			var homeView = new HomeView();
			ContactApp.getLayoutRegion('CONTENT').show(homeView);
			this.updateActiveNav('Home');
		},
		/**
		 * Fired by the contacts, contacts/:id and contacts/:new routes. If an contactId is provided opens
		 * the Contact page/form. If no contactId is provided it loads the contacts page.
		 * @param  {String} contactId The ID or 'new' for the contact clicked
		 */
		contacts: function(contactId) {
			if (!contactId) {
				var contactsTableView = new ContactsTableView();
				ContactApp.getLayoutRegion('CONTENT').show(contactsTableView);
			}else{
				var contactView = new ContactView({'@unid': contactId});
				ContactApp.getLayoutRegion('CONTENT').show(contactView);
			}
			this.updateActiveNav('Contacts');
		},
		/**
		 * Update the active status of the links in the navbar based on the page.
		 * @param  {String} page - The page name the router is navigating to
		 */
		updateActiveNav: function(page) {
			var menuItems = $('.navbar .nav').children();
			menuItems.removeClass('active');
			menuItems.each(function (idx, menuItem) {
				var linkText = $(menuItem).find('a').text();
				if (linkText.indexOf(page) > -1) {
					$(menuItem).addClass('active');
				}
			});
		}
	});

	return AppRouter;

});
