define([
	'marionette',
	'app/views/contactstableitem.js',
	'app/collections/contactscollection.js',
	'text!app/templates/ContactsTable.html'
], function(Marionette, ContactsCollectionItem, ContactsCollection, ContactsTableHtml) {

	/**
	 * The table which will contain all the contacts
	 */
	var ContactsTableView = Backbone.Marionette.CompositeView.extend({
		tagName: 'div',
		className: 'panel panel-primary',
		template: _.template(ContactsTableHtml, this.model),
		/**
		 * The ChildView to render for each item in the collection
		 * @type {ContactsCollectionItem}
		 */
		childView: ContactsCollectionItem,
		/**
		 * Defines the CSS Selector to find the element to place the child elements into
		 * @type {String}
		 */
		childViewContainer: 'tbody',
		/**
		 * The Constructor. Creates a new collection and calls fetch
		 */
		initialize: function(options) {
			this.collection = new ContactsCollection();
			this.collection.fetch({
				data: {
					count: 100
				}
			});
		}
	});

	return ContactsTableView;

});