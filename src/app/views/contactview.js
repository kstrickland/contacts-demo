define([
	'marionette',
	'app/models/contactmodel.js',
	'app/views/formessageview.js',
	'text!app/templates/ContactForm.html'
], function(Marionette, Contact, FormMsgView, ContactFormHtml) {

	/**
	 * This is the Contact form.
	 */
	var ContactView = Backbone.Marionette.ItemView.extend({
		tagName: 'div',
		className: 'panel panel-primary',
		template: _.template(ContactFormHtml, this.model),
		/**
		 * Define what to do when the below events are fired
		 * @type {Object}
		 */
		events: {
			'click #deleteContact': 'deleteContact',
			'click #saveContact': 'saveContact',
			'click #closeContact': 'closeContact',
			'keyup input.form-control': 'updateModel'
		},
		/**
		 * The Constructor. If this isn't a new model, perform a fetch to get the model from the server
		 */
		initialize: function(options) {
			var that = this;
			if (options['@unid'] && options['@unid'] !== 'new') {
				this.model = new Contact({'@unid': options['@unid']});
				this.model.fetch({
					success: function(model, response, options) {
						that.render();
					},
					error: function(model, response, options) {
						console.error('Contact.fetch.error, args=',arguments);
					}
				});
			}else{
				this.model = new Contact();
			}
		},
		/**
		 * Listener for the keyup event on all the fields. This updates the model
		 * @param  {Event} evt The event object
		 */
		updateModel: function(evt) {
			var targetInput = $(evt.target);
			var fieldName = targetInput.attr('name');
			this.model.set(fieldName, targetInput.val());
		},
		/**
		 * Listener for the click event on the save link
		 * @param  {Event} evt The Event Object
		 */
		saveContact: function(evt) {
			evt.preventDefault();
			this.model.save(null, {
				success: function(model, response, options) {
					console.log('success, args=',arguments);
				},
				error: function(model, response, options) {
					var msgOptions = null;
					if (response.status === 201) {
						var location = options.xhr.getResponseHeader('Location');
						var newUnid = location.substring(location.lastIndexOf('/'));
						msgOptions = {
							styleClass: 'alert-success',
							message: 'Contact Successfully Created! Redirecting you to the new Contact'
						};
						var successCreateMsgView = new FormMsgView(msgOptions);
						successCreateMsgView.render();
						setTimeout(function() {
							ContactApp.router.navigate('#/contacts' + newUnid);
						},3000);
					}else if (response.status >= 200 && response.status < 400) {
						msgOptions = {
							styleClass: 'alert-success',
							message: 'Contact Successfully Saved!'
						};
						var successMsgView = new FormMsgView(msgOptions);
						successMsgView.render();
					}else if (response.status >= 400) {
						msgOptions = {
							styleClass: 'alert-danger',
							message: 'Contact NOT Saved! Error: ' + response.status
						};
						var failMsgView = new FormMsgView(msgOptions);
						failMsgView.render();
					}
				}
			});
		},
		/**
		 * The click listener on the Delete link
		 * @param  {Event} evt The Event Object
		 */
		deleteContact: function(evt) {
			evt.preventDefault();
			var that = this;
			var confirmDel = confirm('Really Delete the Contact for ' + this.model.getFullName());
			if (confirmDel) {
				this.model.destroy({
					wait: true,
					dataType: 'text',
					success: function(model, response, options) {
						that.$el.fadeOut('slow', function() {
							ContactApp.router.navigate('#/contacts', {trigger: true});
						});
					}
				});
			}
		},
		/**
		 * The click listener on the close link
		 * @param  {Event} evt The Event object
		 */
		closeContact: function(evt) {
			evt.preventDefault();
			if (this.model.isDirty) {
				var reallyClose = confirm('You have made changes. Closing will discard your changes. Proceed?');
				if (reallyClose) {
					ContactApp.router.navigate('#/contacts', {trigger: true});
				}
			}else{
				ContactApp.router.navigate('#/contacts', {trigger: true});
			}
		}
	});

	return ContactView;

});
