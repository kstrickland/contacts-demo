define([
	'marionette',
	'text!app/templates/Home.html'
], function(Marionette, HomeHtml) {

	/**
	 * The view for the Home Page. This creates a Bootstrap Well, and then places the template
	 * inside the Well.
	 */
	var HomeView = Backbone.Marionette.ItemView.extend({
		/**
		 * The HTML tag this view will create. The Template html will be placed inside this element
		 * @type {String}
		 */
		tagName: 'div',
		/**
		 * The classes to be added to the tagName
		 * @type {String}
		 */
		className: 'well text-center',
		/**
		 * This is where underscore copiles the Template. By default if a model is defined in this view, the
		 * underscore templating engine will use that model to fill in any javascript variables
		 * encountered in the template HTML
		 * @type {Underscore.Template}
		 * @property {HTML} HomeHtml - The HTML to pass to underscore.template
		 */
		template: _.template(HomeHtml),
		initialize: function(options) {

		}
	});

	return HomeView;

});
