define([
	'marionette',
	'app/models/contactmodel.js',
	'text!app/templates/ContactsTableItem.html'
], function(Marionette, Contact, ContactsTableItemHtml) {

	/**
	 * The view for each item in a collection of contacts
	 */
	var ContactsCollectionItem = Backbone.Marionette.ItemView.extend({
		tagName: 'tr',
		/**
		 * This defines attributes to add to the tr element created by this view
		 * @return {Object} Object of html attributes to add
		 */
		attributes: function() {
			var attrs = {
				'unid': this.model.get('@unid')
			};
			return attrs;
		},
		template: _.template(ContactsTableItemHtml, this.model),
		/**
		 * Define the events this view listens for on itself
		 * @type {Object}
		 */
		events: {
			'click #deleteCollectionContact': 'deleteContact',
			'click': 'openContact'
		},
		/**
		 * The Constructor
		 */
		initialize: function(options) {
			this.model = options.model;
		},
		/**
		 * The click listener for the row this view creates. Need to be able to distinguish between a row
		 * click and a delete click
		 * @param  {Event} evt The event object
		 */
		openContact: function(evt) {
			var unid = this.$el.attr('unid');
			// Don't open the contact if the delete icon is clicked
			if (evt.target.tagName.toLowerCase() === 'td') {
				ContactApp.router.navigate('#/contacts/' + unid, {trigger: true});
			}
		},
		/**
		 * The click listener on the delete icon link. Offer a confirmation before actually deleting
		 * @param  {Event} evt The Event object
		 */
		deleteContact: function(evt) {
			evt.preventDefault();
			var that = this;
			var confirmDel = confirm('Really Delete the Contact for ' + this.model.getFullName());
			if (confirmDel) {
				this.model.destroy({
					wait: true,
					dataType: 'text',
					success: function(model, response, options) {
						that.$el.fadeOut();
					},
					error: function(model, response, options) {
						console.error('destroy failed!, args=',arguments);
					}
				});
			}
		}
	});

	return ContactsCollectionItem;

});
