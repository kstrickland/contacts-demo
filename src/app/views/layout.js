define([
	'marionette',
	'text!app/templates/Layout.html'
], function(Marionette, LayoutHtml) {

	/**
	 * The Layout view. This defines the layout and provides a Region for all other content to be added to
	 */
	var LayoutView = Backbone.Marionette.LayoutView.extend({
		template: _.template(LayoutHtml),
		/**
		 * The Regions this layout provides. Formatted like regionName: CSSSelector
		 * @type {Object}
		 */
		regions: {
			CONTENT: '#siteContent'
		},
		/**
		 * The Constructor
		 */
		initialize: function(options) {

		}
	});

	return LayoutView;

});
