define([
	'marionette',
	'settings',
	'app/router.js',
	'app/views/layout.js'
], function(Marionette, Settings, Router, LayoutView) {

	var ContactApp = Backbone.Marionette.Application.extend({
		initialize: function(options) {

		},
		/**
		 * A Helper function to get a region from the LayoutView
		 * @param  {String} regionName The name of the region in the LayoutView
		 * @return {Marionette.Region}            The Region object
		 */
		getLayoutRegion: function(regionName) {
			return this.layoutView.getRegion(regionName);
		},
		/**
		 * Listens for the ajax complete event and then checks if a Domino Login page was returned.
		 * If a login page is returned, navigate to that page and provide the redirecto parameter so
		 * after the login we're redirected back to our current url.
		 */
		setupAjaxComplete: function() {
			$(document).bind('ajaxComplete', function(evt, xhr, xhrOptions){
				var response = xhr.responseText.toLowerCase();
				var authUrl = Settings.AUTHENTICATION_URL.toLowerCase();
				if (response.indexOf('action="' + authUrl + '"') > -1) {
					location.href = authUrl + '&redirectto=' + location.href;
				}
			});
		},
		/**
		 * Called by the start() function. This runs before the application is started. It creates a
		 * LayoutView, renders it and prepends it's html to the body tag of index.html
		 */
		onBeforeStart: function() {
			this.setupAjaxComplete();
			this.layoutView = new LayoutView();
			$('body').prepend(this.layoutView.render().el);
		},
		/**
		 * Called byt the start() function. This runs after the application is started. It creates a
		 * router instance and starts Backbone.history. This is required for the router to function
		 */
		onStart: function() {
			this.router = new Router();
			Backbone.history.start();
		}
	});

	return ContactApp;

});
