## Demo Contacts Application

This is a demo Backbone Marionette application. It is a very simple contacts app

### Setup

#### Requirements

If you want to make changes and build this application. The following are requirements to do that. However if you just want to run the application, use the notesin9-contacts directory as your on-disk-project.

* node.js installed
* grunt-cli installed
* bower installed

#### Configuration

* Clone the repository for this project somewhere onto your machine
* Setup Source Control using the notesin9-contacts directory as your on-disk-project (ODP)
* Open a command line window and change to the directory this project was cloned to
* Type: npm install - Installs the required node modules
* Type: bower install - Installs the required JavaScript dependencies
* Type: grunt setup - Moves the JavaScript, CSS, Fonts, etc files from bower_components into the appropriate directory
* Type: grunt watch - Sets up a watch task so anytime we save a file it builds the app into our ODP. Press ctrl-c to stop the watch task.
* You are now ready to start writing code

### Writing Code

Once you've completed the configuration above and you're ready to make changes to this app. After any changes you make and the build completes (ensure you're running the grunt watch task to do this automatically) just sync the NSF with your ODP.

If you want to build the app manually (not via the grunt watch task) from the command line run: grunt buildDev
